// Fill out your copyright notice in the Description page of Project Settings.

//#include "/Engine/Source/Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include "MyGameInstance.h"




// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 1.f;
	Growth = 100;
	LastMovementDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	BaseSpeed = MovementSpeed;
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	FVector FoodLocation(rand() % 1821 - 910, rand() % 1821 - 910, 0);
	FTransform FoodTransform(FoodLocation);
	AFood* SpawnedFood = GetWorld()->SpawnActor<AFood>(FoodClass, FoodTransform);
	auto ScoreFromGI = Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(this));
	if (ScoreFromGI)
	{
		Score = ScoreFromGI->ScoreGI;
	}
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(0, 0, 0);
		if (SnakeElements.Num() != 0)
		{
			NewLocation = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
		}
		switch (LastMovementDirection)
		{
		case EMovementDirection::UP:
			NewLocation.X -= ElementSize;
			break;
		case EMovementDirection::DOWN:
			NewLocation.X += ElementSize;
			break;
		case EMovementDirection::LEFT:
			NewLocation.Y -= ElementSize;
			break;
		case EMovementDirection::RIGHT:
			NewLocation.Y += ElementSize;
			break;
		}
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();

		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}


	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement,AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst= ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this,bIsFirst );
		}
	}
}

void ASnakeBase::Statistic() 
{
	auto ScoreFromGI = Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(this));
	if (ScoreFromGI)
	{
	Amount += 1;
	ScoreFromGI->ScoreGI += Growth;
	ChangeSpeed();
	Score = ScoreFromGI->ScoreGI;
	}
}

void ASnakeBase::ChangeSpeed()
{

	switch (Amount)
	{
	case 5:
		Growth *= 2;
		MovementSpeed /= 1.2;
		SetActorTickInterval(MovementSpeed);
		break;
	case 10:
		Growth *= 2;
		MovementSpeed /= 1.4;
		SetActorTickInterval(MovementSpeed);
		break;
	case 15:
		ChangeLevel();
		break;
	}
}

void ASnakeBase::ChangeLevel()
{
	UGameplayStatics::OpenLevel(this, "Level2");
}
