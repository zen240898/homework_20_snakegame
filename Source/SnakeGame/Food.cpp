// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Walls.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Snake->Statistic();
			Destroy();
			SpawnFood();
		}
	}
}

void AFood::SpawnFood()
{
	FVector FoodLocation(rand() % 1821 - 910, rand() % 1821 - 910, 0);
	FTransform FoodTransform(FoodLocation);
	AFood* SpawnedFood = GetWorld()->SpawnActor<AFood>(FoodClass, FoodTransform);
}
